package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public final class EmptyTitleException extends AbstractRuntimeException {

    private static final String message = "Error! Name is empty...";

    public EmptyTitleException() {
        super(message);
    }

}
