package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyEmailException extends AbstractRuntimeException {

    private final static String message = "Error! E-mail is empty...";

    public EmptyEmailException() {
        super(message);
    }

}
