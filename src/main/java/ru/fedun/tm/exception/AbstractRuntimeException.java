package ru.fedun.tm.exception;

public class AbstractRuntimeException extends RuntimeException {

    public AbstractRuntimeException(final String message) {
        super(message);
    }

}
