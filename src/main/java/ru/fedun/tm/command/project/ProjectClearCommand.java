package ru.fedun.tm.command.project;

import ru.fedun.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Clear project list";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

}
