package ru.fedun.tm.command.project.remove;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Project project = serviceLocator.getProjectService().removeOneById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println();
    }

}
