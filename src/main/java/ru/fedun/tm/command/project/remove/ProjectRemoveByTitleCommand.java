package ru.fedun.tm.command.project.remove;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectRemoveByTitleCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-title";
    }

    @Override
    public String description() {
        return "Remove project by title.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER TITLE:]");
        final String title = TerminalUtil.nextLine();
        if (title == null || title.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        final Project project = serviceLocator.getProjectService().removeOneByTitle(userId, title);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println();
    }

}
