package ru.fedun.tm.command.project;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER PROJECT TITLE]");
        final String title = TerminalUtil.nextLine();
        System.out.println("[ENTER PROJECT DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, title, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
