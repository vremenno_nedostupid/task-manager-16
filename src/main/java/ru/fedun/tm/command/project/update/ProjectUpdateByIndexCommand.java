package ru.fedun.tm.command.project.update;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt();
        final Project project = serviceLocator.getProjectService().getOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("ENTER TITLE:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated =
                serviceLocator.getProjectService().updateByIndex(userId, index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
