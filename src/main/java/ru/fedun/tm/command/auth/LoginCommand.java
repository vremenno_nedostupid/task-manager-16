package ru.fedun.tm.command.auth;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String description() {
        return "Login in task manager.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
        System.out.println();
    }

}
