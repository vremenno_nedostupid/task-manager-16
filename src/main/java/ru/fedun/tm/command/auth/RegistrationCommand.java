package ru.fedun.tm.command.auth;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.util.TerminalUtil;

public final class RegistrationCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String description() {
        return "Registry in task manager.";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER FIRST NAME:]");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL:]");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, firstName, lastName, email);
        System.out.println("[OK]");
        System.out.println();
    }

}
