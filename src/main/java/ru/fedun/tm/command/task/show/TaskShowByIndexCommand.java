package ru.fedun.tm.command.task.show;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-view_by-index";
    }

    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = serviceLocator.getTaskService().getOneByIndex(userId, index);
        if (task == null) throw new AccessDeniedException();
        System.out.println("ID: " + task.getId());
        System.out.println("TITLE: " + task.getTitle());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
