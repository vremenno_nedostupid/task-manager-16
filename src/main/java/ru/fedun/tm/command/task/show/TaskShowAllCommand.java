package ru.fedun.tm.command.task.show;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Task;

import java.util.List;

public final class TaskShowAllCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
