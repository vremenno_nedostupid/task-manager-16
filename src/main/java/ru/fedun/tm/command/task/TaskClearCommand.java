package ru.fedun.tm.command.task;

import ru.fedun.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Clear task list";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

}
