package ru.fedun.tm.command.task.update;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextInt();
        final Task task = serviceLocator.getTaskService().getOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
        }
        System.out.println("ENTER TITLE:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
