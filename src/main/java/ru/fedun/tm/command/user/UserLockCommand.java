package ru.fedun.tm.command.user;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "lock-user";
    }

    @Override
    public String description() {
        return "Lock user in system.";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER USER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
