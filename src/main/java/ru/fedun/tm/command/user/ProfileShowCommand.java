package ru.fedun.tm.command.user;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.exception.notfound.UserNotFoundException;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.enumerated.Role;

public class ProfileShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "profile";
    }

    @Override
    public String description() {
        return "Show profile info.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROFILE INFO]");
        final String userId = serviceLocator.getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new UserNotFoundException();

        final String login = user.getLogin();
        if (login == null || login.isEmpty()) System.out.println("Login: Unknown");
        else System.out.println("Login: " + login);

        final String firstName = user.getFirstName();
        if (firstName == null || firstName.isEmpty()) System.out.println("First name: Unknown");
        else System.out.println("First Name: " + firstName);

        final String secondName = user.getSecondName();
        if (secondName == null || secondName.isEmpty()) System.out.println("Second name: Unknown");
        else  System.out.println("Second Name: " + secondName);

        final String email = user.getEmail();
        if (email == null || email.isEmpty()) System.out.println("E-mail: Unknown");
        else System.out.println("E-mail: " + email);

        final Role role = user.getRole();
        System.out.println("Role " + role.getDisplayName());
        System.out.println("[OK]");
        System.out.println();
    }

}
