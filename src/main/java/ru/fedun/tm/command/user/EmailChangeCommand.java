package ru.fedun.tm.command.user;

import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.util.TerminalUtil;

public class EmailChangeCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "change-email";
    }

    @Override
    public String description() {
        return "Change profile e-mail.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE EMAIL]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NEW EMAIL:]");
        final String newEmail = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateMail(userId, newEmail);
        System.out.println("[OK]");
        System.out.println();
    }

}
