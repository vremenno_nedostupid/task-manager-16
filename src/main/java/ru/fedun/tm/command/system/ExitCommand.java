package ru.fedun.tm.command.system;

import ru.fedun.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "Shutdown program.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
