package ru.fedun.tm.command.system;

import ru.fedun.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
        System.out.println();
    }
}
