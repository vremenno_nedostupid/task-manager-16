package ru.fedun.tm.command.system;

import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

public final class CommandsViewCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Display program commands.";
    }

    @Override
    public void execute() {
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (AbstractCommand command : commands) {
            System.out.println(command.name());
        }
    }

}
