package ru.fedun.tm.command.system;

import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

public final class ArgumentsViewCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-arg";
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Display program arguments.";
    }

    @Override
    public void execute() {
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
    }

}
