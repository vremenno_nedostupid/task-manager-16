package ru.fedun.tm.util;

import ru.fedun.tm.exception.empty.EmptyValueException;

public interface HashUtil {

    String SECRET = "1234234234";

    Integer ITERATION = 35345;

    static String salt(final String value) {
        if (value == null) throw new EmptyValueException();
            String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    static String md5(final String value) {
        if (value == null) throw new EmptyValueException();
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(value.getBytes());
            StringBuffer sb = new StringBuffer();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
