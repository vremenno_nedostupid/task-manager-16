package ru.fedun.tm.repository;

import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.exception.notfound.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) {
                return user;
            }
        }
        throw new UserNotFoundException();
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) {
                return user;
            }
        }
        throw new UserNotFoundException();
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        remove(user);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        remove(user);
        return user;
    }

}
