package ru.fedun.tm.repository;

import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.command.auth.LoginCommand;
import ru.fedun.tm.command.auth.LogoutCommand;
import ru.fedun.tm.command.auth.RegistrationCommand;
import ru.fedun.tm.command.project.ProjectClearCommand;
import ru.fedun.tm.command.project.ProjectCreateCommand;
import ru.fedun.tm.command.project.remove.ProjectRemoveByIdCommand;
import ru.fedun.tm.command.project.remove.ProjectRemoveByIndexCommand;
import ru.fedun.tm.command.project.remove.ProjectRemoveByTitleCommand;
import ru.fedun.tm.command.project.show.ProjectShowAllCommand;
import ru.fedun.tm.command.project.show.ProjectShowByIdCommand;
import ru.fedun.tm.command.project.show.ProjectShowByIndexCommand;
import ru.fedun.tm.command.project.show.ProjectShowByTitleCommand;
import ru.fedun.tm.command.project.update.ProjectUpdateByIdCommand;
import ru.fedun.tm.command.project.update.ProjectUpdateByIndexCommand;
import ru.fedun.tm.command.system.*;
import ru.fedun.tm.command.task.TaskClearCommand;
import ru.fedun.tm.command.task.TaskCreateCommand;
import ru.fedun.tm.command.task.remove.TaskRemoveByIdCommand;
import ru.fedun.tm.command.task.remove.TaskRemoveByIndexCommand;
import ru.fedun.tm.command.task.remove.TaskRemoveByTitleCommand;
import ru.fedun.tm.command.task.show.TaskShowAllCommand;
import ru.fedun.tm.command.task.show.TaskShowByIdCommand;
import ru.fedun.tm.command.task.show.TaskShowByIndexCommand;
import ru.fedun.tm.command.task.show.TaskShowByTitleCommand;
import ru.fedun.tm.command.task.update.TaskUpdateByIdCommand;
import ru.fedun.tm.command.task.update.TaskUpdateByIndexCommand;
import ru.fedun.tm.command.user.*;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final static Class[] COMMANDS = new Class[]{
            LoginCommand.class, LogoutCommand.class, RegistrationCommand.class, ProfileShowCommand.class,
            EmailChangeCommand.class, PasswordChangeCommand.class, TaskCreateCommand.class, TaskClearCommand.class,
            TaskShowAllCommand.class, TaskShowByIdCommand.class, TaskShowByTitleCommand.class,
            TaskShowByIndexCommand.class, TaskRemoveByIdCommand.class, TaskRemoveByIndexCommand.class,
            TaskRemoveByTitleCommand.class, TaskUpdateByIdCommand.class, TaskUpdateByIndexCommand.class,
            ProjectCreateCommand.class, ProjectClearCommand.class, ProjectShowAllCommand.class,
            ProjectShowByIdCommand.class, ProjectUpdateByIndexCommand.class, ProjectShowByTitleCommand.class,
            ProjectShowByIndexCommand.class, ProjectRemoveByIdCommand.class, ProjectRemoveByIndexCommand.class,
            ProjectRemoveByTitleCommand.class, ProjectUpdateByIdCommand.class, HelpCommand.class, AboutCommand.class,
            SystemInfoCommand.class, VersionCommand.class, CommandsViewCommand.class, ArgumentsViewCommand.class,
            UserLockCommand.class, UserRemoveCommand.class, UserUnlockCommand.class, ExitCommand.class
    };

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        for (final Class clazz : COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                commandList.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public List<AbstractCommand> getCommands() {
        return commandList;
    }

}
