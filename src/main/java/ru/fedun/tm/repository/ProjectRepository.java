package ru.fedun.tm.repository;

import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.notfound.ProjectNotFound;
import ru.fedun.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements ICrudRepository<Project> {

    private final List<Project> projects = new ArrayList<>();

    public void add(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        project.setUserId(userId);
        projects.add(project);
    }

    public void remove(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projects.remove(project);
    }

    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final List<Project> result = new ArrayList<>();
        for (final Project project : projects){
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final List<Project> result = findAll(userId);
        projects.removeAll(result);
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        throw new ProjectNotFound();
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projects.get(index);
    }

    @Override
    public Project findOneByTitle(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        for (final Project project : projects) {
            if (title.equals(project.getTitle())) return project;
        }
        throw new ProjectNotFound();
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFound();
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFound();
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByTitle(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = findOneByTitle(userId, title);
        if (project == null) throw new ProjectNotFound();
        remove(userId, project);
        return project;
    }

}
