package ru.fedun.tm.service;

import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.api.service.ICommandService;
import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public List<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}
