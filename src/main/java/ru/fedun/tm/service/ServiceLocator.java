package ru.fedun.tm.service;

import ru.fedun.tm.api.service.IAuthService;
import ru.fedun.tm.api.service.ICommandService;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;

public interface ServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ICommandService getCommandService();

    ICrudService<Task> getTaskService();

    ICrudService<Project> getProjectService();

}
