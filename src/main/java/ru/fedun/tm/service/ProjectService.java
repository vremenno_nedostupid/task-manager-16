package ru.fedun.tm.service;

import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyTitleException;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.incorrect.IncorrectIndexException;
import ru.fedun.tm.exception.notfound.ProjectNotFound;

import java.util.List;

public class ProjectService implements ICrudService<Project> {

    private final ICrudRepository<Project> projectRepository;

    public ProjectService(ICrudRepository<Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String title, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = new Project();
        project.setTitle(title);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = new Project();
        project.setTitle(title);
        projectRepository.add(userId, project);
    }

    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @Override
    public Project getOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Override
    public Project getOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project getOneByTitle(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        return projectRepository.findOneByTitle(userId, title);
    }

    @Override
    public Project updateById(final String userId, final String id, final String title, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Project project = getOneById(userId, id);
        if (project == null) throw new ProjectNotFound();
        project.setId(id);
        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(
            final String userId,
            final Integer index,
            final String title,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        final Project project = getOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFound();
        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project removeOneByTitle(final String userId, final String title) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (title == null || title.isEmpty()) throw new EmptyTitleException();
        return projectRepository.findOneByTitle(userId, title);
    }

}
