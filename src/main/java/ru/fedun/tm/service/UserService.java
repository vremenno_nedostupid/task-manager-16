package ru.fedun.tm.service;

import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.exception.empty.*;
import ru.fedun.tm.exception.incorrect.IncorrectBehaviourException;
import ru.fedun.tm.exception.incorrect.IncorrectPasswordException;
import ru.fedun.tm.exception.notfound.UserNotFoundException;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password, final String firstName, final String secondName) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPassException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyLoginException();
        if (secondName == null || secondName.isEmpty()) throw new EmptyPassException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setRole(Role.USER);
        return userRepository.add(user);
    }

    @Override
    public User create(
            final String login,
            final String password,
            final String firstName,
            final String secondName,
            final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPassException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password, firstName, secondName);
        if (user == null) throw new IncorrectBehaviourException();
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(
            final String login,
            final String password,
            final String firstName,
            final String secondName,
            final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPassException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password, firstName, secondName);
        if (user == null) throw new IncorrectBehaviourException();
        user.setRole(role);
        return user;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User updateMail(String userId, String email) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = findById(userId);
        if (user == null) throw new AccessDeniedException();
        user.setEmail(email);
        return user;
    }

    @Override
    public User updatePassword(final String userId, final String password, final String newPassword) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (password == null || password.isEmpty()) throw new EmptyPassException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPassException();
        final User user = findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!HashUtil.salt(password).equals(user.getPasswordHash())) throw new IncorrectPasswordException();
        user.setPasswordHash(HashUtil.salt(newPassword));
        return user;
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User removeUser(final String userId, final User user) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        return user;
    }

}
