package ru.fedun.tm.constant;

public interface TerminalConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String INFO = "info";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";

    String TASK_CREATE = "task-create";

    String TASK_CLEAR = "task-clear";

    String TASK_LIST = "task-list";

    String PROJECT_CREATE = "project-create";

    String PROJECT_CLEAR = "project-clear";

    String PROJECT_LIST = "project-list";

    String TASK_VIEW_BY_ID = "task-view-by-id";

    String TASK_VIEW_BY_INDEX = "task-view-by-index";

    String TASK_VIEW_BY_TITLE = "task-view-by-title";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String TASK_REMOVE_BY_TITLE = "task-remove-by-title";

    String PROJECT_VIEW_BY_ID = "project-view-by-id";

    String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String PROJECT_VIEW_BY_TITLE = "project-view-by-title";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String PROJECT_REMOVE_BY_TITLE = "project-remove-by-title";

    String LOGIN = "login";

    String LOGOUT = "logout";

    String REGISTRY = "registry";

    String PROFILE = "profile";

    String DELETE_PROFILE = "delete-profile";

    String CHANGE_EMAIL = "change-email";

    String CHANGE_PASSWORD = "change-password";

    String EXIT = "exit";

}
