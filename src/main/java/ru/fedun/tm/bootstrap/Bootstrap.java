package ru.fedun.tm.bootstrap;

import ru.fedun.tm.api.repository.ICommandRepository;
import ru.fedun.tm.api.repository.ICrudRepository;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.api.service.IAuthService;
import ru.fedun.tm.api.service.ICommandService;
import ru.fedun.tm.api.service.ICrudService;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.command.auth.LoginCommand;
import ru.fedun.tm.command.auth.LogoutCommand;
import ru.fedun.tm.command.auth.RegistrationCommand;
import ru.fedun.tm.command.project.ProjectClearCommand;
import ru.fedun.tm.command.project.ProjectCreateCommand;
import ru.fedun.tm.command.project.remove.ProjectRemoveByIdCommand;
import ru.fedun.tm.command.project.remove.ProjectRemoveByIndexCommand;
import ru.fedun.tm.command.project.remove.ProjectRemoveByTitleCommand;
import ru.fedun.tm.command.project.show.ProjectShowAllCommand;
import ru.fedun.tm.command.project.show.ProjectShowByIdCommand;
import ru.fedun.tm.command.project.show.ProjectShowByIndexCommand;
import ru.fedun.tm.command.project.show.ProjectShowByTitleCommand;
import ru.fedun.tm.command.project.update.ProjectUpdateByIdCommand;
import ru.fedun.tm.command.project.update.ProjectUpdateByIndexCommand;
import ru.fedun.tm.command.system.*;
import ru.fedun.tm.command.task.TaskClearCommand;
import ru.fedun.tm.command.task.TaskCreateCommand;
import ru.fedun.tm.command.task.remove.TaskRemoveByIdCommand;
import ru.fedun.tm.command.task.remove.TaskRemoveByIndexCommand;
import ru.fedun.tm.command.task.remove.TaskRemoveByTitleCommand;
import ru.fedun.tm.command.task.show.TaskShowAllCommand;
import ru.fedun.tm.command.task.show.TaskShowByIdCommand;
import ru.fedun.tm.command.task.show.TaskShowByIndexCommand;
import ru.fedun.tm.command.task.show.TaskShowByTitleCommand;
import ru.fedun.tm.command.task.update.TaskUpdateByIdCommand;
import ru.fedun.tm.command.task.update.TaskUpdateByIndexCommand;
import ru.fedun.tm.command.user.*;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Task;
import ru.fedun.tm.exception.empty.EmptyCommandException;
import ru.fedun.tm.repository.CommandRepository;
import ru.fedun.tm.repository.ProjectRepository;
import ru.fedun.tm.repository.TaskRepository;
import ru.fedun.tm.repository.UserRepository;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.service.*;
import ru.fedun.tm.util.TerminalUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICrudRepository<Task> taskRepository = new TaskRepository();

    private final ICrudService<Task> taskService = new TaskService(taskRepository);

    private final ICrudRepository<Project> projectRepository = new ProjectRepository();

    private final ICrudService<Project> projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        registry(new LoginCommand());
        registry(new LogoutCommand());
        registry(new RegistrationCommand());
        registry(new ProfileShowCommand());
        registry(new EmailChangeCommand());
        registry(new PasswordChangeCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowAllCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByTitleCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByTitleCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowAllCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectShowByTitleCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByTitleCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());
        registry(new CommandsViewCommand());
        registry(new ArgumentsViewCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
        registry(new ExitCommand());
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void initUsers() {
        userService.create("test", "test", "Ivan", "Ivanov");
        userService.create("admin", "admin", "Petr", "Petrov", Role.ADMIN);
    }

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println();
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                runWithCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void runWithCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) throw new EmptyCommandException();
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new EmptyCommandException();
        authService.checkRole(command.roles());
        command.execute();
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        runWithArg(arg);
        return true;
    }

    private void runWithArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand argument = arguments.get(arg);
        argument.execute();
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public ICrudService<Task> getTaskService() {
        return taskService;
    }

    public ICrudService<Project> getProjectService() {
        return projectService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}
