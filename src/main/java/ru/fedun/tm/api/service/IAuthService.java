package ru.fedun.tm.api.service;

import ru.fedun.tm.enumerated.Role;

public interface IAuthService {

    String getUserId();

    void login(String login, String password);

    void registry(String login, String password, String firstName, String secondName, String email);

    void logout();

    boolean isAuth();

    void checkRole(Role[] roles);

}
