package ru.fedun.tm.api.repository;

import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getCommands();

}
