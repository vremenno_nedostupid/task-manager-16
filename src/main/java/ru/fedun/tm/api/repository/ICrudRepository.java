package ru.fedun.tm.api.repository;

import java.util.List;

public interface ICrudRepository<T> {

    List<T> findAll(String userId);

    void clear(String userId);

    void add(String userId, T o);

    void remove(String userId, T o);

    T findOneById(String userId, String id);

    T findOneByIndex(String userId, Integer index);

    T findOneByTitle(String userId, String title);

    T removeOneById(String userId, String id);

    T removeOneByIndex(String userId, Integer index);

    T removeOneByTitle(String userId, String title);

}
