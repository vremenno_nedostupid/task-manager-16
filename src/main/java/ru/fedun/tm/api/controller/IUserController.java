package ru.fedun.tm.api.controller;

public interface IUserController {

    void showUserInfo();

    void deleteProfile();

    void changePassword();

    void changeEmail();

}
