# PROJECT INFO

TASK MANAGER

# DELEVOPER INFO

**NAME**: FEDUN ALEXANDER 

**E-MAIL**: sasha171998@gmail.com

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# TECHNOLOGY STACK

- Java 
- Intellij IDEA 2020.3 Ultimate
- Git
- Maven

# HARDWARE

- CPU: AMD Ryzen 5 or higher
- RAM: 8GB or more

# PROGRAM BUILD
```bash
mvn package
```

# PROGRAM RUN 
```bash
java -jar ./task-manager.jar
```

# CI / CD CURRENT BUILD
https://gitlab.com/vremenno_nedostupid/task-manager-16/-/pipelines